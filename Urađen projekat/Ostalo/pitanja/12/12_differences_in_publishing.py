import conf_global, conf_local, paper, contractor, match, operator

class YearCount:
    def __init__(self, department):
        self.years = []
        for i in range(2000, 2017):
            self.years.append([i, 0])
        self.department = department

    def add_year(self, year):
        if not year or not year.isnumeric():
            return
        year = int(year)

        if 2000 <= int(year) <= 2016:
            self.years[year-2000][1] += 1

    def __str__(self):
        s = "Departman: " + self.department + '\n'
        for i in range(2000, 2017):
            s += 'Godina, vrednost: ' + str(i) + ', ' + str(self.years[i-2000][1]) + '\n'
        return s


def is_checked_author(author, checkedAuthors):
    for checkedA in checkedAuthors:
        if match.match_name(author, checkedA.fullname):
            return checkedA
    return None


authors = contractor.get_contractors()
departments = contractor.get_departments()
basePaper = {department: YearCount(department) for department in departments}
baseLocalConf = {department: YearCount(department) for department in departments}
baseGlobalConf = {department: YearCount(department) for department in departments}
papers = paper.get_papers()
localconfs = conf_local.get_conf_local()
globalconfs = conf_global.get_conf_global()


for p in papers:
    for a in p.authors:
        name = is_checked_author(a, authors)
        if not name:
            continue
        val = basePaper[name.department]
        if p.year:
            val.add_year(p.year)

for lc in localconfs:
    for a in lc.authors:
        name = is_checked_author(a, authors)
        if not name:
            continue
        val = baseLocalConf[name.department]
        if lc.year:
            val.add_year(lc.year)

for gc in globalconfs:
    for a in gc.authors:
        name = is_checked_author(a, authors)
        if not name:
            continue
        val = baseGlobalConf[name.department]
        if gc.year:
            val.add_year(gc.year)


with open('12_differences_in_publishing.txt', mode='w', encoding='utf-8') as f:
    f.write('godina: | časopis | domaća konferencija | strana konferencija\n')
    for i in range(len(basePaper)):
        bpE = list(basePaper.values())[i]
        bpL = list(baseLocalConf.values())[i]
        bpG = list(baseGlobalConf.values())[i]
        f.write(str(list(basePaper.keys())[i]) + ': \n')
        for j in range(2000, 2017):
            f.write(str(bpE.years[j-2000][0]) + ": | " + str(bpE.years[j-2000][1]) + ' | ' + str(bpL.years[j-2000][1]) + ' | ' + str(bpG.years[j-2000][1]) + '\n')

with open('12_differences_in_publishing_sum.txt', mode='w', encoding='utf-8') as f:
    f.write('godina: | časopis | konferencije\n')
    for i in range(len(basePaper)):
        bpE = list(basePaper.values())[i]
        bpL = list(baseLocalConf.values())[i]
        bpG = list(baseGlobalConf.values())[i]
        f.write(str(list(basePaper.keys())[i]) + ': \n')
        for j in range(2000, 2017):
            f.write(str(bpE.years[j-2000][0]) + ": | " + str(bpE.years[j-2000][1]) + ' | ' + str(bpL.years[j-2000][1] + bpG.years[j-2000][1]) + '\n')