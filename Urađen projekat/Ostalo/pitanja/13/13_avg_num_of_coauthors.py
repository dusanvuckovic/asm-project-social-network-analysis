import conf_global, conf_local, paper, contractor, match, operator

class Coauthors:

    def __init__(self, name):
        self.name = name
        self.coauth_num = 0
        self.papers = 0
        self.avg = 0

    def add_coauthors(self, coauth_num):
        self.coauth_num += coauth_num
        self.papers += 1
        self.avg = self.coauth_num / self.papers


    def __str__(self):
        s = self.name + "\n"
        s += "Broj koautora: " + str(self.coauth_num) + '\n'
        s += "Broj radova: " + str(self.papers) + "\n"
        s += "Prosek: " + str(self.avg) + '\n\n'
        return s


def is_checked_author(author, checkedAuthors):
    for checkedA in checkedAuthors:
        if match.match_name(author, checkedA):
            return checkedA
    return None

contractors = contractor.get_author_names()
ca_pap, ca_local, ca_global = Coauthors('Časopisi'), Coauthors('Lokalne konferencije'), Coauthors('Globalne konferencije')

papers = paper.get_papers()
for p in papers:
    for author in p.authors:
        if is_checked_author(author, contractors):
            ca_pap.add_coauthors(len(p.authors) - 1)
            break

localconf = conf_local.get_conf_local()
for lc in localconf:
    for author in lc.authors:
        if is_checked_author(author, contractors):
            ca_local.add_coauthors(len(lc.authors) - 1)
            break

globalconf = conf_global.get_conf_global()
for gc in globalconf:
    for author in gc.authors:
        if is_checked_author(author, contractors):
            ca_global.add_coauthors(len(gc.authors) - 1)
            break

with open('13_avg_num_of_coauthors.txt', mode='w', encoding='utf-8') as f:
    f.write(str(ca_pap))
    f.write(str(ca_local))
    f.write(str(ca_global))
