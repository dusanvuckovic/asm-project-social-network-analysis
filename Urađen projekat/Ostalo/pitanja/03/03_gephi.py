import conf_global, conf_local, paper, contractor, match, operator, csv, itertools

def is_in_DB(employees, name):
    for emp in employees:
        if match.match_name(emp, name):
            return emp
    return False


contractors = contractor.get_contractors()
employees = [x.fullname for x in contractors]


names_in = set()
with open('03_gephi_edges_papers.csv', mode='w', newline='', encoding='utf-8') as f:
    papers = paper.get_papers()
    csvr = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
    csvr.writerow(['Source'] + ['Target'] + ['Title'] + ['Journal'] + ['Year'] + ['Pages'] + ['IF'] + ['IF1BF'] + ['IF2BF'] + ['IF1AF'])
    for p in papers:
        for (str1, str2) in itertools.combinations(p.authors, 2):
            name1 = is_in_DB(employees, str1)
            if name1:
                name2 = is_in_DB(employees, str2)
                if name2:
                    names_in.add(name1)
                    names_in.add(name2)
                    csvr.writerow([name1] + [name2] + [p.title_paper] + [p.title_journal] + [p.year] + [p.pages] + [p.impf] + [p.if_1bf] + [p.if_2bf] + [p.if_1af])

with open('03_gephi_nodes_papers.csv', mode='w', newline='', encoding='utf-8') as f:
    csvr = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
    csvr.writerow(['Id'] + ['Department'])
    for c in contractors:
        if c.fullname in names_in:
            csvr.writerow([c.fullname] + [c.department])


names_in = set()
with open('03_gephi_edges_localconf.csv', mode='w', newline='', encoding='utf-8') as f:
    localconfs = conf_local.get_conf_local()
    csvr = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
    csvr.writerow(['Source'] + ['Target'] + ['Title'] + ['Conference'] + ['Year'] + ['Pages'])
    for lc in localconfs:
        for (str1, str2) in itertools.combinations(lc.authors, 2):
            name1 = is_in_DB(employees, str1)
            if name1:
                name2 = is_in_DB(employees, str2)
                if name2:
                    names_in.add(name1)
                    names_in.add(name2)
                    csvr.writerow([name1] + [name2] + [lc.title_paper] + [lc.title_conf] + [lc.year] + [lc.pages])
with open('03_gephi_nodes_localconf.csv', mode='w', newline='', encoding='utf-8') as f:
    csvr = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
    csvr.writerow(['Id'] + ['Department'])
    for c in contractors:
        if c.fullname in names_in:
            csvr.writerow([c.fullname] + [c.department])

names_in = set()
with open('03_gephi_edges_globalconf.csv', mode='w', newline='', encoding='utf-8') as f:
    globalconfs = conf_global.get_conf_global()
    csvr = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
    csvr.writerow(['Source'] + ['Target'] + ['Title'] + ['Conference'] + ['Year'] + ['Pages'])
    for gc in globalconfs:
        for (str1, str2) in itertools.combinations(gc.authors, 2):
            name1 = is_in_DB(employees, str1)
            if name1:
                name2 = is_in_DB(employees, str2)
                if name2:
                    names_in.add(name1)
                    names_in.add(name2)
                    csvr.writerow([name1] + [name2] + [gc.title_paper] + [gc.title_conf] + [gc.year] + [gc.pages])
with open('03_gephi_nodes_globalconf.csv', mode='w', newline='', encoding='utf-8') as f:
    csvr = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
    csvr.writerow(['Id'] + ['Department'])
    for c in contractors:
        if c.fullname in names_in:
            csvr.writerow([c.fullname] + [c.department])

names_in = set()
with open('03_gephi_edges_total.csv', mode='w', newline='', encoding='utf-8') as f:
    papers = paper.get_papers()
    localconfs = conf_local.get_conf_local()
    globalconfs = conf_global.get_conf_global()
    csvr = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
    csvr.writerow(['Source'] + ['Target'] + ['Title'] + ['Year'] + ['Pages'])
    for p in papers:
        for (str1, str2) in itertools.combinations(p.authors, 2):
            name1 = is_in_DB(employees, str1)
            if name1:
                name2 = is_in_DB(employees, str2)
                if name2:
                    names_in.add(name1)
                    names_in.add(name2)
                    csvr.writerow([name1] + [name2] + [p.title_paper] + [p.year] + [p.pages])
    for lc in localconfs:
        for (str1, str2) in itertools.combinations(lc.authors, 2):
            name1 = is_in_DB(employees, str1)
            if name1:
                name2 = is_in_DB(employees, str2)
                if name2:
                    names_in.add(name1)
                    names_in.add(name2)
                    csvr.writerow([name1] + [name2] + [lc.title_paper] + [lc.year] + [lc.pages])
    for gc in globalconfs:
        for (str1, str2) in itertools.combinations(gc.authors, 2):
            name1 = is_in_DB(employees, str1)
            if name1:
                name2 = is_in_DB(employees, str2)
                if name2:
                    names_in.add(name1)
                    names_in.add(name2)
                    csvr.writerow([name1] + [name2] + [gc.title_paper] + [gc.year] + [gc.pages])

with open('03_gephi_nodes_total.csv', mode='w', newline='', encoding='utf-8') as f:
    csvr = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
    csvr.writerow(['Id'] + ['Department'])
    for c in contractors:
       if c.fullname in names_in:
           csvr.writerow([c.fullname] + [c.department])
