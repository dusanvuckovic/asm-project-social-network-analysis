import conf_global, conf_local, paper, contractor, match, operator


class Coauthors:

    def __init__(self, name):
        self.name = name
        self.coauth_num = 0
        self.papers = 0
        self.avg = 0

    def add_coauthors(self, coauth_num):
        self.coauth_num += coauth_num
        self.papers += 1
        self.avg = self.coauth_num / self.papers


    def __str__(self):
        s = self.name + "\n"
        s += "Broj koautora: " + str(self.coauth_num) + '\n'
        s += "Broj radova: " + str(self.papers) + "\n"
        s += "Prosek: " + str(self.avg) + '\n\n'
        return s


contractors = contractor.get_contractors()
coauthors = [Coauthors(x.fullname) for x in contractors]

papers = paper.get_papers()
for p in papers:
    for author in p.authors:
        for cauth in coauthors:
            if match.match_name(cauth.name, author):
                cauth.add_coauthors(len(p.authors)-1)
coauthors.sort(key=operator.attrgetter('avg'), reverse=True)


with open('pitanje1_radovi.txt', mode='w', encoding='utf-8') as f:
    i = 1
    f.write('rang: prosečni broj koautora | broj objavljenih radova\n')
    for auth in coauthors:
        f.write('#' + str(i) + ': ' + str(auth.name) + ' | ' + format(auth.avg, '.2f') + ' | ' + str(auth.papers) + "\n")
        i += 1

coauthors = [Coauthors(x.fullname) for x in contractors]

conflocals = conf_local.get_conf_local()
for p in conflocals:
    for author in p.authors:
        for cauth in coauthors:
            if match.match_name(cauth.name, author):
                cauth.add_coauthors(len(p.authors)-1)
coauthors.sort(key=operator.attrgetter('avg'), reverse=True)


with open('pitanje1_domacekonferencije.txt', mode='w', encoding='utf-8') as f:
    i = 1
    f.write('rang: prosečni broj koautora | broj objavljenih radova\n')
    for auth in coauthors:
        f.write('#' + str(i) + ': ' + str(auth.name) + ' | ' + format(auth.avg, '.2f') + ' | ' + str(auth.papers) + "\n")
        i += 1

confglobals = conf_global.get_conf_global()
for p in confglobals:
    for author in p.authors:
        for cauth in coauthors:
            if match.match_name(cauth.name, author):
                cauth.add_coauthors(len(p.authors)-1)
coauthors.sort(key=operator.attrgetter('avg'), reverse=True)


with open('pitanje1_stranekonferencije.txt', mode='w', encoding='utf-8') as f:
    i = 1
    f.write('rang: prosečni broj koautora | broj objavljenih radova\n')
    for auth in coauthors:
        f.write('#' + str(i) + ': ' + str(auth.name) + ' | ' + format(auth.avg, '.2f') + ' | ' + str(auth.papers) + "\n")
        i += 1
