import conf_global, conf_local, paper, contractor, match, operator


class EventCount:
    def __init__(self, name):
        self.name = name
        self.num = 1

    def increment(self):
        self.num += 1

    def __str__(self):
        s = "Ime: " + self.name
        s += '\nPonavljanja: ' + str(self.num)
        return s

def get_highest(lst):
    lst.sort(key=operator.attrgetter('num'), reverse=True)
    return lst[0]

scorePaper = {}
scoreLocal = {}
scoreInternational = {}

for p in paper.get_papers():
    if p.title_journal not in scorePaper:
        scorePaper[p.title_journal] = EventCount(p.title_journal)
    else:
        scorePaper[p.title_journal].increment()

for l in conf_local.get_conf_local():
    if l.title_conf not in scoreLocal:
        scoreLocal[l.title_conf] = EventCount(l.title_conf)
    else:
        scoreLocal[l.title_conf].increment()

for g in conf_global.get_conf_global():
    if g.title_conf:
        if g.title_conf not in scoreInternational:
            scoreInternational[g.title_conf] = EventCount(g.title_conf)
        else:
            scoreInternational[g.title_conf].increment()

with open('pitanje14.txt', newline='\n', encoding='utf-8', mode='w') as f:
    f.write(str(get_highest(list(scorePaper.values()))))
    f.write('\n')
    f.write(str(get_highest(list(scoreLocal.values()))))
    f.write('\n')
    f.write(str(get_highest(list(scoreInternational.values()))))
