import conf_global, conf_local, paper, contractor, match, operator


class Productive:

    def __init__(self, name, department):
        self.name = name
        self.department = department
        self.papers = 0

    def add_papers(self):
        self.papers += 1


    def __str__(self):
        s = self.name + "\n"
        s += "Ime: " + str(self.name) + '\n'
        s += "Katedra: " + str(self.department) + "\n"
        s += "Broj radova: " + str(self.papers)  + '\n\n'
        return s

def split_deps(productives):
    res = {}
    for p in productives:
        if p.department in res:
            res[p.department].append(p)
        else:
            res[p.department] = [p]
    most_productive = []
    for dept in res:
        as_list = list(res[dept])
        as_list.sort(key=operator.attrgetter('papers'), reverse=True)
        most_productive.append(as_list[0])
    return most_productive


contractors = contractor.get_contractors()
productives = [Productive(x.fullname, x.department) for x in contractors]


papers = paper.get_papers()
for p in papers:
    for author in p.authors:
        for prod in productives:
            if match.match_name(prod.name, author):
                prod.add_papers()

most_productive = split_deps(productives)
most_productive.sort(key=operator.attrgetter('papers'), reverse=True)

with open('pitanje2_radovi.txt', mode='w', encoding='utf-8') as f:
    f.write('Katedra: Ime i prezime | broj objavljenih radova\n')
    for auth in most_productive:
        f.write(str(auth.department) + ': ' + str(auth.name) +  ' | ' + str(auth.papers) + "\n")

########################################################################

productives = [Productive(x.fullname, x.department) for x in contractors]

conflocals = conf_local.get_conf_local()
for c in conflocals:
    for author in c.authors:
        for prod in productives:
            if match.match_name(prod.name, author):
                prod.add_papers()

most_productive = split_deps(productives)
most_productive.sort(key=operator.attrgetter('papers'), reverse=True)

with open('pitanje2_domacekonferencije.txt', mode='w', encoding='utf-8') as f:
    f.write('Katedra: Ime i prezime | broj objavljenih radova\n')
    for auth in most_productive:
        f.write(str(auth.department) + ': ' + str(auth.name) +  ' | ' + str(auth.papers) + "\n")

##############################################################

productives = [Productive(x.fullname, x.department) for x in contractors]

confglobals = conf_global.get_conf_global()
for p in confglobals:
    for author in p.authors:
        for prod in productives:
            if match.match_name(prod.name, author):
                prod.add_papers()


most_productive = split_deps(productives)
most_productive.sort(key=operator.attrgetter('papers'), reverse=True)

with open('pitanje2_stranekonferencije.txt', mode='w', encoding='utf-8') as f:
    f.write('Katedra: Ime i prezime | broj objavljenih radova\n')
    for auth in most_productive:
        f.write(str(auth.department) + ': ' + str(auth.name) +  ' | ' + str(auth.papers) + "\n")