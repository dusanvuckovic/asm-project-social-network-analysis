Katedra: Ime i prezime | broj objavljenih radova
Katedra za telekomunikacije: Slobodan Jovičić | 36
Katedra za elektroenergetske sisteme: Nikola Rajaković | 30
Katedra za signale i sisteme: Željko Đurović | 28
Katedra za mikroelektroniku i tehničku fiziku: Dejan Raković | 27
Katedra za opštu elektrotehniku: Branimir Reljin | 26
Katedra za energetske pretvarače i pogone: Borislav Jeftenić | 25
Katedra za elektroniku: Predrag Pejović | 22
Katedra za računarsku tehniku i informatiku: Boško Nikolić | 15
Katedra za primenjenu matematiku: Branko Malešević | 13
Katedra za opšte obrazovanje: Miloš Đurić | 6
