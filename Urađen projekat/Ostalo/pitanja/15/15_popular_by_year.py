import conf_global, conf_local, paper, contractor, match, operator


class YearCount:
    def __init__(self, department):
        self.years = []
        for i in range(2000, 2017):
            self.years.append([i, 0])
        self.department = department

    def add_year(self, year):
        if not year or not year.isnumeric():
            return
        year = int(year)

        if 2000 <= int(year) <= 2016:
            self.years[year-2000][1] += 1

    def __str__(self):
        s = "Departman: " + self.department + '\n'
        for i in range(2000, 2017):
            s += 'Godina, vrednost: ' + str(i) + ', ' + str(self.years[i-2000][1]) + '\n'
        return s


def is_checked_author(author, checkedAuthors):
    for checkedA in checkedAuthors:
        if match.match_name(author, checkedA.fullname):
            return checkedA
    return None


authors = contractor.get_contractors()
departments = contractor.get_departments()
base = {department: YearCount(department) for department in departments}
papers = paper.get_papers()
for p in papers:
    for a in p.authors:
        name = is_checked_author(a, authors)
        if not name:
            continue
        val = base[name.department]
        if p.year:
            val.add_year(p.year)

with open('15_popular_by_year_paper_total.txt', mode='w', encoding='utf-8') as f:
    for k, v in base.items():
        f.write(v.department + ':\n')
        for nl in v.years:
          f.write(str(nl) + '\n')
        f.write('\n')

with open('15_popular_by_year_paper.txt', mode='w', encoding='utf-8') as f:
    for k, v in base.items():
        v.years.sort(key=operator.itemgetter(1), reverse=True)
        max = 0
        newlist = []
        for yr in v.years:
            if (yr[1]) >= max:
                max = yr[1]
                newlist.append(yr)
            else:
                break
        f.write(v.department + ': ')
        for nl in newlist:
          f.write(str(nl) + ' ')
        f.write('\n')

authors = contractor.get_contractors()
departments = contractor.get_departments()
base = {department: YearCount(department) for department in departments}
localconfs = conf_local.get_conf_local()
for lc in localconfs:
    for a in lc.authors:
        name = is_checked_author(a, authors)
        if not name:
            continue
        val = base[name.department]
        if lc.year:
            val.add_year(lc.year)

with open('15_popular_by_year_localconf_total.txt', mode='w', encoding='utf-8') as f:
    for k, v in base.items():
        f.write(v.department + ':\n')
        for nl in v.years:
          f.write(str(nl) + '\n')
        f.write('\n')

with open('15_popular_by_year_localconf.txt', mode='w', encoding='utf-8') as f:
    for k, v in base.items():
        v.years.sort(key=operator.itemgetter(1), reverse=True)
        max = 0
        newlist = []
        for yr in v.years:
            if (yr[1]) >= max:
                max = yr[1]
                newlist.append(yr)
            else:
                break
        f.write(v.department + ': ')
        for nl in newlist:
          f.write(str(nl) + ' ')
        f.write('\n')


authors = contractor.get_contractors()
departments = contractor.get_departments()
base = {department: YearCount(department) for department in departments}
globalconfs = conf_global.get_conf_global()
for gc in globalconfs:
    for a in gc.authors:
        name = is_checked_author(a, authors)
        if not name:
            continue
        val = base[name.department]
        if gc.year:
            val.add_year(gc.year)

with open('15_popular_by_year_globalconf_total.txt', mode='w', encoding='utf-8') as f:
    for k, v in base.items():
        f.write(v.department + ':\n')
        for nl in v.years:
          f.write(str(nl) + '\n')
        f.write('\n')

with open('15_popular_by_year_globalconf.txt', mode='w', encoding='utf-8') as f:
    for k, v in base.items():
        v.years.sort(key=operator.itemgetter(1), reverse=True)
        max = 0
        newlist = []
        for yr in v.years:
            if (yr[1]) >= max:
                max = yr[1]
                newlist.append(yr)
            else:
                break
        f.write(v.department + ': ')
        for nl in newlist:
          f.write(str(nl) + ' ')
        f.write('\n')
