import csv


def replace_double(line):
    if line.endswith('}'):
        line = line[:-1]
    if line.startswith('{'):
        line = line[1:]

    line.strip()
    while line.find("  ") != -1:
        line = line.replace("  ", " ")
    while line.find('"') != -1:
        line = line.replace('"', '')
    while line.find('}') != -1:
        line = line.replace('}', '')
    return line


class ConfGlobal:
    def __init__(self, row):
        self.ID_conf = row[0]
        self.authors = [replace_double(x) for x in str.split(row[12], ',')]
        self.title_paper = row[1]
        self.place = row[3]
        self.title_conf = row[2]
        self.year = row[11]
        pages1, pages2 = row[6], row[7]
        if pages1.isnumeric() and pages2.isnumeric() and int(pages2) > int(pages1):
            self.pages = str(int(pages2) - int(pages1) + 1)
        else:
            self.pages = ""

    def __str__(self):
        s = ''
        s += 'ID konferencije: '       + self.ID_conf
        s += "\nAutori: "
        s += ", ".join(self.authors)
        s += '\nNaziv rada: '       + self.title_paper
        s += '\nMesto: ' + self.place
        s += '\nNaziv konferencije: ' + self.title_conf
        s += '\nGodina održavanja: ' + self.year
        s += '\nBroj stranica: ' + self.pages

        return s


def get_conf_global():
    with open('lat_konf_medj.csv', newline='', encoding='utf-8') as f:
        reader = csv.reader(f)
        my_list = list(reader)[1:]
        return [ConfGlobal(x) for x in my_list]