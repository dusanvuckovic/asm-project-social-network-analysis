from excel_data import *

CONTRACTS = "ugov.xlsx"
MAGAZINES = "cas.xlsx"
CONFERENCES_DOMESTIC = "konf_dom.xlsx"
CONFERENCES_FOREIGN = "konf_medj.xlsx"

workbook_to_latin(CONFERENCES_DOMESTIC)
print_out_sheet(CONFERENCES_DOMESTIC)

workbook_to_latin(CONFERENCES_FOREIGN)
print_out_sheet(CONFERENCES_FOREIGN)
