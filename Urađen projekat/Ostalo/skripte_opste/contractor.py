import csv
import itertools
import match


class Contractor:
    def __init__(self, row):
        self.department = row[0]
        self.firstname = row[1]
        self.lastname = row[2]
        self.fullname = row[3]

    def __str__(self):
        s = ''
        s += 'Katedra: '       + self.department
        s += '\nIme: '           + self.firstname
        s += '\nPrezime: '       + self.lastname
        s += '\nIme i prezime: ' + self.fullname

        return s


def get_contractors():
    with open('lat_ugov.csv', newline='', encoding='utf-8') as f:
        reader = csv.reader(f)
        my_list = list(reader)[1:]
        return [Contractor(x) for x in my_list]


def get_author_names():
    contractors = get_contractors()
    return [contractor.fullname for contractor in contractors]


def get_departments():
    contractors = get_contractors()
    departments = []
    for c in contractors:
        if c.department not in departments:
            departments.append(c.department)
    return departments