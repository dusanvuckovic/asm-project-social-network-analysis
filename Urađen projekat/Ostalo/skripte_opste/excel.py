import transliterate
import openpyxl


def print_out_sheet(text):
    sheet = openpyxl.load_workbook(text).active
    for row in sheet.iter_rows():
        for cell in row:
            if cell.value and type(cell.value) == type(''):
                print(cell.value + "|", sep=' ', end="")
        print()


def workbook_to_latin(text):
    book = openpyxl.load_workbook(text)
    w = book.active
    for row in w.iter_rows():
        for cell in row:
            if type('') == type(cell.value) and transliterate.is_cyrillic(cell.value):
                cell.value = transliterate.to_latin(cell.value)
    book.save('lat_' + text)


