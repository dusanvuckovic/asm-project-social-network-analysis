matcher_simple = {
    'č':['č', 'c'],
    'ć':['ć', 'c'],
    'đ':['đ', 'd'],
    'š':['š', 's'],
    'ž':['ž', 'z'],
    'Č':['Č', 'C'],
    'Ć':['Ć', 'C'],
    'Đ':['Đ', 'D'],
    'Š':['Š', 'S'],
    'Ž':['Ž', 'Z']
}

matcher_complex = {
    'đ':['đ', 'dj'],
    'Đ':['Đ', 'Dj']
}


def test_char_simple(char1, char2):
    if char1 == char2:
        return True
    if char1 in matcher_simple and char2 in matcher_simple[char1]:
        return True
    if char2 in matcher_simple and char1 in matcher_simple[char2]:
        return True
    return False


def match_simple(name1, name2):
    if name1 == name2:
        return True
    for i in range(len(name1)):
        if not test_char_simple(name1[i], name2[i]):
            return False
    return True


def match_complex(name1, name2):
    len1, len2 = len(name1), len(name2)
    sl1, sl2 = 0, 0;
    maxL = max(len1, len2)
    while (sl1 < len1 and sl2 < len2):
        char1, char2 = name1[sl1], name2[sl2]
        if char1 in matcher_complex:
            if char2+name2[sl2+1] in matcher_complex[char1]:
                sl1 += 1
                sl2 += 2
                continue
        if char2 in matcher_complex:
            if char1+name1[sl1+1] in matcher_complex[char2]:
                sl1 += 2
                sl2 += 1
                continue
        if test_char_simple(char1, char2):
            sl1 += 1
            sl2 += 1
            continue
        return False
    return True



def match_name(name1, name2):
    if name1 == name2:
        return True
    len1, len2 = len(name1), len(name2)
    match = match_simple(name1, name2) if len1 == len2 else match_complex(name1, name2)
    return match