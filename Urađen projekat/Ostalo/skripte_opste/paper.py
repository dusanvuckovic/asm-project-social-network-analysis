import csv


def replace_double(line):
    line.strip()
    while line.find("  ") != -1:
        line = line.replace("  ", " ")
    return line


class Paper:
    def __init__(self, row):
        self.ID_paper = row[0]
        self.authors = [replace_double(x) for x in str.split(row[1], ',')]
        self.title_paper = row[3]
        self.ID_journal = row[4]
        self.title_journal = row[6]
        self.year = row[7]
        pages1, pages2 = row[11], row[12]
        if pages1.isnumeric() and pages2.isnumeric() and int(pages2) > int(pages1):
            self.pages = str(int(pages2) - int(pages1) + 1)
        else:
            self.pages = ""
        self.impf = row[14]
        self.if_1bf = row[15]
        self.if_2bf = row[16]
        self.if_1af = row[17]

    def __str__(self):
        s = ''
        s += 'ID rada: '       + self.ID_paper
        s += "\nAutori: "
        s += ", ".join(self.authors)
        s += '\nNaziv rada: '       + self.title_paper
        s += '\nID časopisa: ' + self.ID_journal
        s += '\nNaziv časopisa: ' + self.title_journal
        s += '\nGodina objavljivanja: ' + self.year
        s += '\nBroj stranica: ' + self.pages
        s += '\nIF: ' + self.impf
        s += '\nIF 1 pre: ' + self.if_1bf
        s += '\nIF 2 pre: ' + self.if_2bf
        s += '\nIF 1 posle: ' + self.if_1af

        return s


def get_papers():
    with open('lat_cas.csv', newline='', encoding='utf-8') as f:
        reader = csv.reader(f)
        my_list = list(reader)[1:]
        return [Paper(x) for x in my_list]